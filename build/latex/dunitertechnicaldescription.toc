\babel@toc {english}{}
\contentsline {chapter}{\numberline {1}Introduction}{3}{chapter.1}%
\contentsline {chapter}{\numberline {2}State of the art: Bitcoin case}{5}{chapter.2}%
\contentsline {section}{\numberline {2.1}Monetary creation of Bitcoin: a space\sphinxhyphen {}time asymmetry}{5}{section.2.1}%
\contentsline {subsection}{\numberline {2.1.1}Spatial asymmetry}{5}{subsection.2.1.1}%
\contentsline {subsection}{\numberline {2.1.2}Temporal\sphinxhyphen {}asymmetry}{6}{subsection.2.1.2}%
\contentsline {subsection}{\numberline {2.1.3}A solution}{6}{subsection.2.1.3}%
\contentsline {section}{\numberline {2.2}Proof\sphinxhyphen {}of\sphinxhyphen {}Work mining: a power race}{6}{section.2.2}%
\contentsline {subsection}{\numberline {2.2.1}What about Proof of Stake?}{6}{subsection.2.2.1}%
\contentsline {chapter}{\numberline {3}Duniters Blockchain}{7}{chapter.3}%
\contentsline {section}{\numberline {3.1}Spam countermeasures}{7}{section.3.1}%
\contentsline {subsection}{\numberline {3.1.1}Minimum output amount}{8}{subsection.3.1.1}%
\contentsline {subsection}{\numberline {3.1.2}Limited block size and chainability}{8}{subsection.3.1.2}%
\contentsline {section}{\numberline {3.2}Scaling}{8}{section.3.2}%
\contentsline {subsection}{\numberline {3.2.1}Dynamic block size}{8}{subsection.3.2.1}%
\contentsline {subsection}{\numberline {3.2.2}Lightning Networks}{8}{subsection.3.2.2}%
\contentsline {subsection}{\numberline {3.2.3}Unit base}{9}{subsection.3.2.3}%
\contentsline {chapter}{\numberline {4}Duniter Web of Trust}{11}{chapter.4}%
\contentsline {section}{\numberline {4.1}Basic Principles}{11}{section.4.1}%
\contentsline {section}{\numberline {4.2}Why do we need a Web of Trust?}{12}{section.4.2}%
\contentsline {section}{\numberline {4.3}The importance of having our own certification system}{12}{section.4.3}%
\contentsline {section}{\numberline {4.4}A few foundational concepts on graph theory: a bit of vocabulary}{13}{section.4.4}%
\contentsline {section}{\numberline {4.5}Definition of the Duniter Web of Trust}{13}{section.4.5}%
\contentsline {section}{\numberline {4.6}Exploring the rules behind a Duniter Web of Trust}{14}{section.4.6}%
\contentsline {subsection}{\numberline {4.6.1}Distance rule and referent members (\sphinxstyleliteralintitle {\sphinxupquote {stepMax}} and \sphinxstyleliteralintitle {\sphinxupquote {xPercent}})}{14}{subsection.4.6.1}%
\contentsline {subsection}{\numberline {4.6.2}Rule of the minimum number of certifications needed (\sphinxstyleliteralintitle {\sphinxupquote {sigQty}})}{15}{subsection.4.6.2}%
\contentsline {subsection}{\numberline {4.6.3}The membership renewal rule (\sphinxstyleliteralintitle {\sphinxupquote {msValidity}}, \sphinxstyleliteralintitle {\sphinxupquote {msPeriod}} and \sphinxstyleliteralintitle {\sphinxupquote {msWindow}})}{15}{subsection.4.6.3}%
\contentsline {subsection}{\numberline {4.6.4}Rule of certification lifespan (\sphinxstyleliteralintitle {\sphinxupquote {sigValidity}})}{15}{subsection.4.6.4}%
\contentsline {subsection}{\numberline {4.6.5}Rule of limited supply of active certifications (\sphinxstyleliteralintitle {\sphinxupquote {sigStock}})}{15}{subsection.4.6.5}%
\contentsline {subsection}{\numberline {4.6.6}Rule of the time period between two certification issuances. (\sphinxstyleliteralintitle {\sphinxupquote {sigPeriod}})}{16}{subsection.4.6.6}%
\contentsline {subsection}{\numberline {4.6.7}Expiry of a certification issuance (\sphinxstyleliteralintitle {\sphinxupquote {sigWindow}})}{16}{subsection.4.6.7}%
\contentsline {subsection}{\numberline {4.6.8}Lifespan of a ‘pending’ identity (\sphinxstyleliteralintitle {\sphinxupquote {idtyWindow}})}{16}{subsection.4.6.8}%
\contentsline {section}{\numberline {4.7}Details on some of the WoT’s peculiarities at the genesis block}{16}{section.4.7}%
\contentsline {section}{\numberline {4.8}Why these rules and application cases in the Ğ1}{16}{section.4.8}%
\contentsline {subsection}{\numberline {4.8.1}Distance and maximum size}{16}{subsection.4.8.1}%
\contentsline {subsection}{\numberline {4.8.2}Time is our friend}{18}{subsection.4.8.2}%
\contentsline {subsection}{\numberline {4.8.3}Trust me now, trust me forever? (\sphinxstyleliteralintitle {\sphinxupquote {sigValidity}}, \sphinxstyleliteralintitle {\sphinxupquote {msValidity}})}{19}{subsection.4.8.3}%
\contentsline {subsection}{\numberline {4.8.4}Keeping the pools free of information glut (\sphinxstyleliteralintitle {\sphinxupquote {idtyWindow}}, \sphinxstyleliteralintitle {\sphinxupquote {sigWindow}}, \sphinxstyleliteralintitle {\sphinxupquote {msWindow}})}{19}{subsection.4.8.4}%
\contentsline {subsection}{\numberline {4.8.5}Avoiding single members from ‘knowing too many people’ (\sphinxstyleliteralintitle {\sphinxupquote {sigStock}})}{20}{subsection.4.8.5}%
\contentsline {subsection}{\numberline {4.8.6}Avoiding locking minorities (\sphinxstyleliteralintitle {\sphinxupquote {xpercent}})}{20}{subsection.4.8.6}%
\contentsline {subsection}{\numberline {4.8.7}Spam protection with (\sphinxstyleliteralintitle {\sphinxupquote {msPeriod}})}{20}{subsection.4.8.7}%
\contentsline {chapter}{\numberline {5}Proof of Work with personal difficulty}{21}{chapter.5}%
\contentsline {section}{\numberline {5.1}Why do we need Proof of Work?}{21}{section.5.1}%
\contentsline {section}{\numberline {5.2}Only members can “mine”}{22}{section.5.2}%
\contentsline {section}{\numberline {5.3}How does it work?}{22}{section.5.3}%
\contentsline {subsection}{\numberline {5.3.1}The hash (aka digest)}{22}{subsection.5.3.1}%
\contentsline {subsection}{\numberline {5.3.2}The common difficulty}{22}{subsection.5.3.2}%
\contentsline {subsubsection}{How is difficulty applied?}{22}{subsubsection*.3}%
\contentsline {subsubsection}{The Nonce}{23}{subsubsection*.4}%
\contentsline {section}{\numberline {5.4}Personalised difficulty}{23}{section.5.4}%
\contentsline {subsection}{\numberline {5.4.1}Understanding \sphinxstyleliteralintitle {\sphinxupquote {exFact}}, the exclusion factor}{23}{subsection.5.4.1}%
\contentsline {subsubsection}{What is intended by “the number of members forging”?}{24}{subsubsection*.5}%
\contentsline {subsubsection}{Current window}{24}{subsubsection*.6}%
\contentsline {subsubsection}{exFact and the personalised difficulty}{24}{subsubsection*.7}%
\contentsline {subsection}{\numberline {5.4.2}The handicap}{25}{subsection.5.4.2}%
\contentsline {chapter}{\numberline {6}Conclusion}{27}{chapter.6}%
\contentsline {chapter}{\numberline {7}Sources}{29}{chapter.7}%
