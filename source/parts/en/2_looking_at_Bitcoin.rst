
.. _state-of-the-art-bitcoin-case:

State of the art: Bitcoin case
------------------------------

.. raw:: html

   <!-- source : https://duniter.org/en/theoretical/ -->

Duniter uses the crypto-currency concept introduced by Bitcoin [#Bitcoin]_,
which is to use cryptographic tools such as signatures to create
trustless digital currencies. Duniter fits this definition, but it has
two completely different principles than Bitcoin: the Web of Trust and
the Universal Dividend. These differences are on both monetary and
technical aspects.

.. _monetary-creation-a-space-time-asymmetry:

Monetary creation of Bitcoin: a space-time asymmetry
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Space-time asymmetry refers to the relative access of individuals to
newly created money [#RTM]_. Concretely, most existing currencies (c. 2020)
are both spatially and temporally asymmetrical for their users. Let's
take Bitcoin as an example to understand why.

Spatial asymmetry
^^^^^^^^^^^^^^^^^

When new Bitcoins are created, only some Bitcoin users (the miners) are
given new Bitcoins, while everyone else get nothing. We believe this is
the first injustice. However, some might say:

   "Miners used their electricity and time to get it!"

We answer that this work should not be rewarded by
newly created Bitcoins. New units should be distributed to the whole
community. Miners should be rewared another way, but not by money
issuance. Of course, Bitcoin cannot create money through Basic Income
since Bitcoin users are not strongly identified, and one might benefit
from money creation multiple times if they owned several wallets.
Duniter gets rid of this problem by identifying its users and creating
the same amount of Basic Income to everyone.

Temporal-asymmetry
^^^^^^^^^^^^^^^^^^

Bitcoin has an absolute limit of 21 million BTC (its unit of currency),
which means ever fewer bitcoins will be created over time until no new
BTC are being generated. So, once the first adopters have mined every
bitcoin, how will future joiners get Bitcoins? Just like most of us do
for Euros or Dollars: to get money, they will have to work for the ones
who already own it.

We believe this is the second injustice. Every member of a monetary
community should be equal concerning monetary creation, and get the same
relative amount of money over time, even if they are a late adopter.
Duniter aims to fix this by making the Universal Dividend (a.k.a. UD)
grow by the time according to precise rules, thus making members equal
toward money issuance on a half-lifespan.

Most currencies present one of these two asymmetries, including metal
currencies and mutual credit, as exposed in the RTM.

A solution
^^^^^^^^^^

Bitcoin has taught us that it is possible to create a currency system
allowing one to both create digital money and to exchange it without a
central authority. What we need to change is the way money is issued so
we finally have a symmetrical system. We need **Bitcoin + Universal
Dividend**. But Universal Dividend implies that the community consists
of only identified people. This is where the Web of Trust (WoT) comes
into place.

This concept, introduced by cryptography with the OpenPGP format [#OpenPGP]_,
allows us to identify people in a decentralized manner. It works as
follows: each person creates a personal identity that is linked to its
cyptographic certificate. The identity must be confirmed by others
members who use their own cryptographic key. It is that simple: people
choose who is part of the community and who is not, not a central
authority.

However, Duniter will not use OpenPGP for its cryptographic features:
Elliptic Curves [#EC]_ will be used instead for the conciseness of its
generated keys and its pratical advantages. Duniter has its own Web of
Trust principles, that will be exposed later.

.. _proof-of-work-mining-a-power-race:

Proof-of-Work mining: a power race
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

In Bitcoin Model, the calculation and incentive principles cause a power
race: new Bitcoins are created for the owners of the most numerous,
powerful (and energy-consuming) computers. This leads to a power race an
places the control over the currency in the hands of the richest
hardware owners. We want to make Duniter blockchain validation much less
energy and hardware consuming while keeping a strong level of security.
This will be further explained later. A consequence of this choice is
the participation of low-end hardware in the Duniter network, leading to
a better decentralization of blockchain validation.

What about Proof of Stake?
^^^^^^^^^^^^^^^^^^^^^^^^^^

Proof of stake consensus algorythm was first introduced in 2012 [#PPCoin]_.
The basic principle is to allow the richest wallets to issue blocks,
putting their coin balance as a “stake” they would lose in case of
cheat.

At the time of conceiving Duniter, the PoS algorythms had not been
tested enough to be used as a fundamental base. We did not chose this
consensus principle. Moreover, the principle of allowing owners of large
amounts of money to validate transaction can only lead to placing power
over the currency in the richests hands: this is contrary to the
symmetical principles of a libre currency.

