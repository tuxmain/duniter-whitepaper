
.. _duniters-blockchain:

Duniters Blockchain
-------------------

..   <!-- source : https://duniter.org/en/theoretical/ -->

Duniters Blockchain follows the basic principles of Bitcoins. This is
essential for synchronization between peers, as to prevent double-spend
attacks. However, Duniters Blockchain will store different informations
than Bitcoins.

The basic use of Blockchain will be registering transactions. For this
part, we use the same principles as Bitcoin: transactions have inputs
(spending accounts) and outputs (receiving accounts). But contrary to
Bitcoin, no generation transaction exists: monetary creation happens
only through UD. So, in Duniters Blockchain, Inputs can be either:

-  a former transaction (as in Bitcoin)
-  a Universal Dividend (specific to Duniter).

Duniters Web of Trust is also written in the Blockchain. The identity of
each member gets registered much like transactions are, with a strong
link to the time reference. Thus, the Blockchain is a representation of
a space-time frame of reference, where “space” are members of the WoT
and “time” the basic blockchain units: the blocks. On each point of
time, one can determine which account is legitimate to create the UD,
only with a blockchain analysis.

.. _spam-countermeasures:

Spam countermeasures
~~~~~~~~~~~~~~~~~~~~

An issue of most cryptocurrency projects is to prevent the common ledger
from growing too much. This would require nodes to have a lot of storage
and computing power to be usable. In particular, we don’t want an
attacker to be able to make the Blockchain grow too fast. Most projects
implement transaction fees as a way to prevent this, making the attacker
lose money. We don’t want to introduce this mean: a currency with
automatic fees on transactions is no more neutral since it creates an incentive not to spend the money. Several
countermeasures against such spam attacks are implemented.

..   <!-- see : https://forum.duniter.org/t/sans-frais-de-transaction-comment-resister-aux-attaques/3846/25 (implemented?)-->

Minimum output amount
^^^^^^^^^^^^^^^^^^^^^

.. <!-- This has to be implemented in DUBPv13. -->

Fixing a minimal output amount reduces the power of an attack. Duniter
deals with cents of Ğ1 or 1/1000 of the first UD. An attacker could
create thousand accounts with only 1 UD. To prevent this, a valid
transaction must have output amounts of minimum 1Ğ1. This reduces the
power of an attack by 100.

Limited block size and chainability
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The block size is always limited. While the protocol allows this limit
to evolve to address scaling issues, an attacker cannot register as many
transaction as they wish.

With the same goal to prevent too many transactions to get registered,
while transactions can be “chained” (refer to another transaction in the
same block), the chainability of transactions is limited to 5.

.. _scaling:

Scaling
~~~~~~~

Most of the time, the scaling issue rises for distributed systems that
should work on a very large scale. This is not the case of Duniter, for
multiple reasons:

-  Ğ1 is the first libre currency, and is still experimental on the
   monetary creation principle. We don’t want it to reach the whole
   world, we only want it to work, to validate or invalidate the RTM.
   Moreover, the rules chosen for the Ğ1 WoT should limit its size to
   around 16 million members.
-  Duniter’s aim is to be used to create multiple libre currencies, that
   would fit local or regional economies. As a consequence, it would
   deal with less transactions than if it was a world-scale system. The
   RTM proposes a formula to calculate the exchange rate between two
   currencies, that could be used to create automatic exchanges for a
   member travelling away from their community.

However, Duniter has assets that will help if the number of users and
transactions grow.

Dynamic block size
^^^^^^^^^^^^^^^^^^

While Bitcoin has a fixed block size, Duniters blocks size can evolve.
On low use of the blockchain, the maximal block size is 500 bytes. On
high use of the blockchain, the maximal block size would be 110% of the
average size of the current window blocks(see “personalised difficulty”
part for more information). This way, the blocks are bounded in size,
but can slowly grow if a massive and legitimate use of the blockchain
needs it. The block size (in bytes) is limited as so:

.. math::
   :label: blocksize

   \textrm{blockSize} < \max \left({500 ; \textrm{CEIL}(1.10 \times (\textrm{average block size}))}\right) 

Lightning Networks
^^^^^^^^^^^^^^^^^^

The Lightning Networks [#Lightning]_ allow almost instant and off-chain
transactions. They were first implemented on Lightcoin, and are now on
Bitcoin. One of their benefits is to make the blockchain store a lot of
transactions at once, thus reducing the growth of the blockchain. The
Duniter protocol allows XHX() and CSV() unlock conditions that are
necessary to implement Lightning Networks. While not available yet, this
payment channel might get implemented when needed.

Unit base
^^^^^^^^^

As the Universal Dividend grows exponentially, with time Duniter nodes
would have had to deal with always largest amounts, eventually reaching
the BIGINT limit (SQL databases don't deal with numbers larger
than :math:`2^{63}`). To avoid this, the amounts are expressed with a unit
base in base 10. We want the UD amount to always fit in 4 digits. To
manage it, the ``unitbase`` is updated each time the UD value reaches
100.00: it goes from :math:`99.99*10^{(unitbase)}` to
:math:`10.00*10^{(unitbase+1)}`. All the unit amounts are thus divided by 10.
While this might seem strange, this process has already hapened in state
currencies. Moreover, the amounts expressed in UD will not change.

With a monetary growth of 10% each year and a stable population, such a
change of unit base would happen each 25 years.

