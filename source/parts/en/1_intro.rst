.. Duniter: A libre currency blockchain generator.

.. ===============================================


.. rubric:: Abstract

Many currency principles involve non-equal rights to monetary creation
between humans. We propose a monetary creation based on the Relative
Theory of Money, which guarantee equal monetary creation for each
willing human. This type of currency can be centralised, however, this
could lead to censorship and arbitrary choices of the central
institution. Thus, strongly inspired by Bitcoin example, we want the
currency to be as decentralised as possible, in the transaction network
as in the human identification process. We use a Web of Trust between
living humans for identification. This web of trust allows us to impose
personalised difficulty for transaction validation, keeping the
calculation accessible to low-end hardware and allowing all competent
members to secure the currency.

.. _introduction:

Introduction
------------

Duniter is a software to create and manage “libre currencies”. Libre
currency is a concept defined by S.Laborde in the Relative Theory of
Money (RTM) that was published in 2010. This theory demonstrates the
possibility of an invariant monetary unit: the Universal Dividend.
Doing so, the RTM answers the question:

   How should a currency be created to match the principle of equality
   between all humans, now and between generations?

The results of this demonstration implies a monetary creation:

-  on a regular basis
-  for each human being
-  which amount has to be reassessed on fixed intervals according to a
   fixed formula.

Thus, Duniter project will associate a human to a digital identity. It
will use a Web of Trust with specific rules. As the number of members
may evolve, the Universal Dividend has to be created according to the
formula:

.. math::
   :label: ud

   \mathit{UD}(t+1) = \mathit{UD}(t) + c^2 \times {M(t) \over N(t)}

Duniter is based on a decentralized Blockchain. This technical choice
allows irreversibility of transaction and uncensorability of trades and
identities. While inspired by Bitcoin, Duniter uses a Web of Trust and
the Proof of Work to secure the computation network, thus making
obsolete the power race model used in Bitcoin.

The first currency created through Duniter is Ğ1, pronounced “June”. It
was created on the 8th. March 2017. This whitepaper uses Ğ1 parameters
as examples; however, one can create another libre currency with custom
parameters while still using Duniter software.

In this document, we present how Duniter works and the choices we made.
The informations are relevant for the v13 of the Duniter Protocol.
