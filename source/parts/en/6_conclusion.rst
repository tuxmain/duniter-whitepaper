
.. _conclusion:

Conclusion
----------

.. raw:: html

   <!-- source : https://duniter.org/en/theoretical/ -->

Duniter’s Blockchain can be compared to Bitcoin’s: a common document
retracing the history of the currency. However, Duniter registers not
only trades, but also the history of relationships in the community as a
mean to identify a human to a digital account. This way, Duniter has
information about the fondamental reference of RTM: living humans. A
libre Currency can be issued thanks to the Universal Dividend.

More than that, Duniter proposes a new model for securing a Blockchain
in an efficient and decentralized way. Basing the security on a Web of
Trust with an individualised security makes the calculation rules more
fair. A side effect of this choice is a network consisting mostly of
low-end computers, maintaining a good security and helping
decentralization of calculation.

The ultimate goal of Duniter project is to allow people to participate
in a libre economy, thanks to a libre currency. What is a libre economy?
The Relative Theory of Money defines it through four economic
liberties:

-  The freedom to choose your currency system: because money should not
   be imposed.
-  The freedom to access resources: because we all should have access to
   economic & monetary resources.
-  The freedom to estimate and produce value: because value is a purely
   relative to each individual.
-  The freedom to trade with the money: because we should not be limited
   by the avaible money supply.

Those 4 economic freedoms should be understood together, not
exclusively. Plus, “freedom” has to be understood as “non-nuisance”. So
here, freedom does not mean the right to take all of a resource (like
water source in a desert) so no more is available to the others. Now you
get it, this is the goal: free economy through free currency.
