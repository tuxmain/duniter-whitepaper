#!/bin/bash

# Ce script sert à publier le WhitePaper.
# La première version crée les documents whitepaper.md et whitepaper.html.
# Le doc Markdown complet est volontairement supprimé. Je préfère que les modifications soient faites sur les différentes parties.
# ce script doit être lancé dans le dossier contenant les chapitres.

# dépendances : sphinx-build
hash sphinx-build 2>/dev/null || { echo >&2 "Ce générateur a besoin de la commande sphinx-build pour fonctionner. Mais cette commande n'est pas installée. Fin de l'exécution."; exit 1; }

# clean build
rm -rf build/*

## Compilation HTML des différents chapitres
### (ceci devrait être refactorisé pour le support de davantage de langues)
### version anglaise
cp source/base_index.rst source/index.rst
for i in source/parts/en/*.rst ; do
    cat $i >> source/index.rst
done

sphinx-build -b html source build

if [ -e build/index.html ] ; then
    echo "===== HTML OK ====="
fi

## Compilation PDF
sphinx-build -M latexpdf source build
# deplacement dans le fichier de build
mv ./build/latex/dunitertechnicaldescription.pdf ./build/

#firefox "$PWD/build/whitepaper_en.html"
## add ebook conversion
#hash pandoc 2>/dev/null || { echo >&2 "Ce générateur peut utiliser ebook-convert fourni avec Calibre, disponible dans les dépots apt. Mais cette commande n'est pas installée. Fin de l'exécution."; exit 1; }
#ebook-convert build/whitepaper_fr.html build/whitepaper_fr.mobi
#ebook-convert build/whitepaper_en.html build/whitepaper_en.mobi
#ebook-convert build/whitepaper_fr.html build/whitepaper_fr.fb2
#ebook-convert build/whitepaper_en.html build/whitepaper_en.fb2
#ebook-convert build/whitepaper_fr.html build/whitepaper_fr.epub
#ebook-convert build/whitepaper_en.html build/whitepaper_en.epub
#echo "===== ok ebooks"
#ls -lArth build


# clean fichiers temporaires
if [ -e ./source/index.rst ] ; then
   rm ./source/index.rst
fi
