# Duniter technical description

Rédaction d'une description technique pour le projet Duniter.

Je ne suis pas compétent pour entrer dans les détails du projet.
Cependant, je le suis assez pour rédiger la majeure partie du texte explicatif.
J'invite qui le souhaite à m'accompagner dans cette rédaction.

## Installation et compilation sur système Debian

Les sources sont rédigées en syntaxe RestructuredText et compilées avec Sphinx.

Pour compiler les sources markdown vers les versions html, ebook et pdf vous allez avoir besoin de ces dépendances:

```bash
sudo apt install latexmk pip3
python3 -m pip install sphinx --user
make
```

## TODO
* traduire en Français les chapitres

### Références

D'autres whitepapers, pour observer le ton et les sujets à aborder.
* Bitcoin : https://bitcoin.org/bitcoin.pdf
* Ethereum : https://github.com/ethereum/wiki/wiki/White-Paper
* Nano : https://content.nano.org/whitepaper/Nano_Whitepaper_en.pdf

## Questions :

[la protection contre le spam](https://forum.duniter.org/t/sans-frais-de-transaction-comment-resister-aux-attaques/3846/25) a-t-elle été implémentée ? Sous quelle forme/paramètres ?

## ToDo : suite discussion JaWaKa

### Forme :

* Scientifique : sources en bas (BTC met les numéros de renvois en face des sources)

* liens internes vers graphiques


